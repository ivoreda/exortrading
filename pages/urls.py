from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('about/', views.AboutPageView.as_view(), name='about'),
    path('terms-of-use/', views.TermsPageView.as_view(), name='terms-of-service'),
    path('privacy-policy/', views.PrivacyPolicyPageView.as_view(), name='privacy-policy'),
    path('services/', views.ServicesPageView.as_view(), name='services'),
    path('contact/', views.ContactPageView.as_view(), name='contact-us'),
]