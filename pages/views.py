from django.shortcuts import render
from django.views.generic import TemplateView
# Create your views here.

class HomePageView(TemplateView):
    template_name = 'home.html'

class AboutPageView(TemplateView):
    template_name = 'about.html'

class TermsPageView(TemplateView):
    template_name = 'terms.html'

class PrivacyPolicyPageView(TemplateView):
    template_name = 'privacypolicy.html'

class ServicesPageView(TemplateView):
    template_name = 'services.html'

class ContactPageView(TemplateView):
    template_name = 'contact.html'