# Generated by Django 3.1.5 on 2021-02-08 21:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customuser',
            old_name='age',
            new_name='phone_number',
        ),
    ]
