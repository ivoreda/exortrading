from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser

# Register your models here.

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    list_display = ['email', 'username', 'phone_number', 'first_name', 'last_name']
    fieldsets = (
        (None, {'fields': ('email', 'password', 'username', 'phone_number', 'first_name', 'last_name')}),
        ('Personal info', {'fields': ('identification_front', 'identification_back')}),
        ('Permissions', {'fields': ()}),
    )
    model = CustomUser

admin.site.register(CustomUser, CustomUserAdmin)