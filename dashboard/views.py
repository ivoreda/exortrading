from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView
from . import models
from django.shortcuts import reverse
from django.urls import reverse_lazy
from django.conf import settings

from datetime import datetime, timedelta
from threading import Timer


# Create your views here.
class DashBoardHomeView(ListView):
    model = models.Dashboard
    template_name = 'dashboard-home.html'

    def get_queryset(self):
        #query = self.request.GET.get('q')
        object_list = models.Dashboard.objects.filter(owner=self.request.user)
        return object_list
    
    def add_to_balance(self):
        x = datetime.today()
        y = x.replace(day=x.day, hour=1,minute=0,seconds=0, microseconds=0) + timedelta(days=1)
        delta_t = y-x

        secs = delta_t.total_seconds()

        def add():
            models.Dashboard.total_balance + models.Plans.daily_return
        
        t = Timer(secs, add)
        t.start()


class DashBoardUpdateAccountView(TemplateView):
    template_name = 'update-account.html'

class DashBoardReferUserView(ListView):
    model = models.Dashboard
    template_name = 'refer-user.html'

    def get_queryset(self):
        object_list = models.Dashboard.objects.filter(owner=self.request.user)
        return object_list

class DashBoardSupportView(TemplateView):
    template_name = 'support.html'

class DashBoardPlansView(TemplateView):
    template_name = 'plans.html'

class DashBoardMyPlansView(ListView):
    model = models.Plans
    template_name = 'mplans.html'

class DashBoardDepositsView(ListView):
    model = models.Deposit
    template_name = 'deposits.html'

    def get_queryset(self):
        object_list = models.Deposit.objects.filter(owner=self.request.user)
        return object_list

class NewDepositView(CreateView):
    model = models.Deposit
    template_name = 'new-deposit.html'
    fields = ['payment_method', 'plan', 'amount']

    def form_valid(self, form):
        response = super(NewDepositView, self).form_valid(form)
        self.object.owner = self.request.user
        self.object.save()
        return response

    success_url = reverse_lazy('deposits')

    

   

class NewWithdrawalView(CreateView):
    model = models.Withdrawal
    template_name = 'new-withdrawal.html'
    fields = ['wallet_address', 'amount', 'ssn', 'W_-_2_tax_refund_form']


    def form_valid(self, form):
        response = super(NewWithdrawalView, self).form_valid(form)
        self.object.owner = self.request.user
        self.object.save()
        return response

    success_url = reverse_lazy('withdrawals')

class DashBoardWithdrawalsView(ListView):
    model = models.Withdrawal
    template_name = 'withdrawals.html'

    def get_queryset(self):
        object_list = models.Withdrawal.objects.filter(owner=self.request.user)
        return object_list


# class DashBoardUpdateAccountView(TemplateView):
#     template_name = 'update-account.html'

# class DashBoardUpdateAccountView(TemplateView):
#     template_name = 'update-account.html'

