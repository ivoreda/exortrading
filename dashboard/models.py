from django.db import models
from django.conf import settings
from django.urls import reverse
# Create your models here.

class Dashboard(models.Model):

    ACCOUNT_STATUS = (
                        ('Inactive', 'inactive'),
                        ('Active', 'active')
    )
    
    LABEL_CHOICES = (
                        ('success', 'success'),
                        ('declined', 'danger')
    )

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=settings.AUTH_USER_MODEL)
    account_status = models.CharField(choices=ACCOUNT_STATUS, max_length=200, default='inactive')
    deposit = models.CharField(max_length=200, default=0)
    referral_bonus = models.CharField(max_length=200, default=0)
    total_balance = models.CharField(max_length=200, default=0)
    time_of_deposit = models.TimeField(auto_now_add=True, null=True)
    date_of_deposit = models.DateField(auto_now_add=True, null=True)
    label = models.CharField(choices=LABEL_CHOICES, max_length=20, default='success')

    class Meta:
       unique_together = ['owner', 'deposit', 'account_status', 'referral_bonus','total_balance', 'time_of_deposit', 'date_of_deposit', 'label']

    def __str__(self):
        return str(self.owner)

class Deposit(models.Model):

    PAYMENT_METHOD_CHOICES = (
                            ('BTC', 'BTC'),
    )

    PLANS_CHOICES = (
                    ('Demo', 'Demo'),
                    ('Starter', 'Starter'),
                    ('Gold', 'Gold'),
                    ('Silver', 'Silver'),
                    ('Diamond', 'Diamond'),
                    ('Platinum', 'Platinum'),
                    ('Starter X3', 'Starter X3'),
                    ('Silver X3', 'Silver X3'),
                    ('Gold X3', 'Gold X3'),
    )

    LABEL_CHOICES = (
                    ('verified', 'verified'),
                    ('declined', 'declined'),
                    ('pending', 'pending')
    )

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    amount = models.CharField(max_length=20)
    plan = models.CharField(choices=PLANS_CHOICES, max_length=20, default='demo')
    payment_method = models.CharField(max_length=20, choices=PAYMENT_METHOD_CHOICES, default='BTC')
    status = models.CharField(choices=LABEL_CHOICES, max_length=20, default='pending')
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return str(self.owner)

    def get_absolute_url(self):
        return reverse('deposits')


class Plans(models.Model):

    LABEL_CHOICES = (
                    ('verified', 'verified'),
                    ('declined', 'declined'),
                    ('pending', 'pending')
    )

    PAYMENT_METHOD_CHOICES = (
                            ('BTC', 'BTC'),
                            ('Bank', 'Bank')
    )

    PLANS_CHOICES = (
                    ('Demo', 'Demo'),
                    ('Starter', 'Starter'),
                    ('Gold', 'Gold'),
                    ('Silver', 'Silver'),
                    ('Diamond', 'Diamond'),
                    ('Platinum', 'Platinum'),
                    ('Starter X3', 'Starter X3'),
                    ('Silver X3', 'Silver X3'),
                    ('Gold X3', 'Gold X3'),
    )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    plan_name = models.CharField(choices=PLANS_CHOICES, max_length=20, default='Demo')
    amount = models.CharField(max_length=20)
    expected_return = models.CharField(max_length=20, default=0)
    daily_return = models.CharField(max_length=20, default=0)
    payment_method = models.CharField(max_length=20, choices=PAYMENT_METHOD_CHOICES)
    status = models.CharField(choices=LABEL_CHOICES, max_length=20, default='pending')

    def __str__(self):
        return str(self.owner)
# Continue from here. making models and views for the plans template

class Withdrawal(models.Model):

    LABEL_CHOICES = (
                    ('verified', 'verified'),
                    ('declined', 'declined'),
                    ('pending', 'pending')
    )

    PAYMENT_METHOD_CHOICES = (
                            ('BTC', 'BTC'),
                            ('Bank', 'Bank')
    )

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    amount = models.CharField(max_length=50)
    wallet_address = models.CharField(max_length=30)
    status = models.CharField(choices=LABEL_CHOICES, max_length=20, default='pending')
    date = models.DateField(auto_now_add=True)
    ssn = models.CharField(default=0, max_length=9)
    W2_tax_refund_form = models.FileField(blank=True, null=True)
    
    def __str__(self):
        return str(self.owner)

    def get_absolute_url(self):
        return reverse('withdrawals')