from django.urls import path
from . import views

urlpatterns = [
    path('home/', views.DashBoardHomeView.as_view(), name='dashboard-home'),
    path('update-account/', views.DashBoardUpdateAccountView.as_view(), name='update-account'),
    path('refer-user/', views.DashBoardReferUserView.as_view(), name='refer-user'),
    path('support/', views.DashBoardSupportView.as_view(), name='support'),
    path('buy-plans/', views.DashBoardPlansView.as_view(), name='buy-plans'),
    path('my-plans/', views.DashBoardMyPlansView.as_view(), name='my-plans'),
    path('deposits/', views.DashBoardDepositsView.as_view(), name='deposits'),
    path('withdrawals/', views.DashBoardWithdrawalsView.as_view(), name='withdrawals'),
    path('new-deposit/', views.NewDepositView.as_view(), name='new-deposit'),
    path('new-withdrawal/', views.NewWithdrawalView.as_view(), name='new-withdrawal'),
]