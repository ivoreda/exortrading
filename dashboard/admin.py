from django.contrib import admin
from . import models

# Register your models here.

@admin.register(models.Dashboard)
class DashboardAdmin(admin.ModelAdmin):
    list_display = ['owner', 'total_balance', 'date_of_deposit', 'time_of_deposit']

@admin.register(models.Deposit)
class DepositAdmin(admin.ModelAdmin):
    list_display = ['owner', 'amount', 'status']


@admin.register(models.Plans)
class PlansAdmin(admin.ModelAdmin):
    list_display = ['owner', 'amount', 'status']

@admin.register(models.Withdrawal)
class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ['owner', 'amount', 'status']